function success(pos){
	var lati = pos.coords.latitude,
		longi = pos.coords.longitude,
	    map_options = {
          center: new google.maps.LatLng(lati, longi),
          zoom: 12,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        },
        map = new google.maps.Map(document.getElementById("map_canvas"),
            map_options);
}

function error(e){
	alert(e);
}

function locate(ce){
	navigator.geolocation.getCurrentPosition(success, error);
}

$(window).load(locate);

function choosePic(){
	navigator.camera.getPicture(picSuccess, picError, { quality: 50, 
    destinationType: Camera.DestinationType.FILE_URI }); 

}

function picSuccess(imageURI) {
    var image = document.getElementById("my_image");
    image.src = imageURI;
    //send to a server?
}

function picError(message) {
    alert("Failed because: " + message);
}

$("#get_pic").click(choosePic);



function chooseVid(){

    navigator.device.capture.captureVideo(vidSuccess, vidError, {limit: 1});

}


function vidSuccess(mediaFiles) {
    var video = document.getElementById("my_video");
    video.src = mediaFiles[0].fullPath
    //send to a server?
}

function vidError(message) {
    alert("Failed because: " + message);
}

$("#get_vid").click(chooseVid);

function startBarcode(){
    console.log("scanning");
    try {
        window.plugins.barcodeScanner.scan(function(args) {
            alert("Scanner result: \n" +
                "text: " + args.text + "\n" +
                "format: " + args.format + "\n" +
                "cancelled: " + args.cancelled + "\n");
            /*
            if (args.format == "QR_CODE") {
                window.plugins.childBrowser.showWebPage(args.text, { showLocationBar: false });
            }
            */
            //$("#barcode_info").innerHTML = args.text;
            //console.log(args);
        });
    }catch(ex){
        alert(ex.message);
    }
}
$("#get_barcode").click(startBarcode);
